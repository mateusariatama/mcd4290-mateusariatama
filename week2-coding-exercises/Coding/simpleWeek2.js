//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let Array = [54,-16,80,55,-74,73,26,5,-34,-73,19,63,-55,-61,65,-14,-19,-51,-17,-25]
    let positiveOdd = []
    let negativeEven = []
    for (let i = 0; i < Array.length; i++){
        if (Array[i] > 0 && Array[i]%2 !== 0){
        positiveOdd.push(Array[i])
        }
        else if (Array[i] < 0 && Array[i]%2 === 0){
        negativeEven.push(Array[i])
        }
    }
    
    output += "Positive Odd: " + positiveOdd;
    output += "\n Negative Even: " + negativeEven;
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
    
    let diceOne = 0;
    let diceTwo = 0;
    let diceThree = 0;
    let diceFour = 0;
    let diceFive = 0;
    let diceSix = 0;
    
    for(i = 0; i < 60000; i++){
        let randomDice = Math.floor((Math.random() * 6) + 1)
        if(randomDice === 1){
        diceOne ++;
        }
        else if (randomDice === 2){
        diceTwo ++;
        }
        else if (randomDice === 3){
        diceThree ++;
        }
        else if (randomDice === 4){
        diceFour ++;
        }
        else if (randomDice === 5){
        diceFive ++;
        }
        else if (randomDice === 6){
        diceSix ++;
        }
    }
    
    output += "Frequency of die rolls \n"
    output += "1: " + diceOne + "\n"
    output += "2: " + diceTwo  + "\n"
    output += "3: " + diceThree + "\n"
    output += "4: " + diceFour + "\n"
    output += "5: " + diceFive + "\n"
    output += "6: " + diceSix + "\n"
        
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    
    let diceFace = [0,0,0,0,0,0,0]
    
    for(i = 0; i < 60000; i++){
        let randomDice = Math.floor((Math.random() * 6) + 1)
        if(randomDice === 1){
        diceFace[1] ++;
        }
        else if (randomDice === 2){
        diceFace[2] ++;
        }
        else if (randomDice === 3){
        diceFace[3] ++;
        }
        else if (randomDice === 4){
        diceFace[4] ++;
        }
        else if (randomDice === 5){
        diceFace[5] ++;
        }
        else if (randomDice === 6){
        diceFace[6] ++;
        }
    }
    
    output += "Frequency of die rolls \n"
    output += "1: " + diceFace[1] + "\n"
    output += "2: " + diceFace[2] + "\n"
    output += "3: " + diceFace[3] + "\n"
    output += "4: " + diceFace[4] + "\n"
    output += "5: " + diceFace[5] + "\n"
    output += "6: " + diceFace[6] + "\n"
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    
    var dieRolls = {
        Frequencies: {
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        Total:60000,
        Exceptions: ""
    }
    
    for(var i = 0; i < dieRolls.Total; i++){
        var diceFace = Math.floor((Math.random() * 6) + 1);
        dieRolls.Frequencies[diceFace] ++;
    } 
    
    var expectedValue = 10000;
    var onePercentValue = 0.01 * expectedValue;
    
    for(var index = 1; index <= 6; index ++){
        var difference = dieRolls.Frequencies[index] - expectedValue;
        if (Math.abs(difference) > onePercentValue){
            dieRolls.Exceptions += index + " ";
        }
    }
    
    output += "Frequency of dice rolls \n"
    output += "Total rolls: " + dieRolls.Total + "\n"
    output += "Frequencies: \n"
    
    for (var index in dieRolls.Frequencies){
        output += index + ": " + dieRolls.Frequencies[index] + "\n";
    }
    
    output += "Exceptions: " + dieRolls.Exceptions
        
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    
    let person = {
        name: "Jane",
        income: 127050
    } 
    
    let personIncome = person.income;
    let tax;
    
    if(personIncome > 0 && personIncome <= 18200){
        tax = 0
    }
    else if(personIncome <= 37000){
        tax = 0.19 * (personIncome - 18200)
    }
    else if(personIncome <= 90000){
        tax = 3572 + (0.352 * (personIncome - 37000))
    }
    else if(personIncome <= 180000){
        tax = 20797 + (0.37 * (personIncome - 90000))
    }
    else if(personIncome > 180000){
        tax = 54097 + (0.45 * (personIncome - 180000))
    }
    
    output += person.name + "'s income is: $" + person.income + ", and her tax owed is: $" + tax
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}