// Coding Exercises Week 1
// Name: Mateus Ariatama
// ID: 32030150

// Question 1
let radius = 4;
let circumferenceC = 2*radius*Math.PI;
let result = circumferenceC.toFixed(2);
console.log(result)

// Question 2
let animalString = "cameldogcatlizard";
let andString = " and ";
console.log(animalString.substring(8,11)+andString.substr(0)+animalString.substring(5,8))

// Question 3
let person = {
  firstName: "Kanye",
  lastName: "West",
  birthDate: "8 June 1977",
  annualIncome: "150000000.00",
}
console.log(person.firstName + " " + person.lastName + " " + "was born on" + " " + person.birthDate + " " + "and has an annual income of $" + person.annualIncome)

// Question 4
var number1, number2;

number1 = Math.floor((Math.random() * 10) +1);
number2 = Math.floor((Math.random() * 10) +1);                        

console.log("number1 = " + number1 + " number2 = " +number2)

//HERE
myNumber = number1;
number1 = number2;
number2 = myNumber;
console.log("number1 = " + number1 + " number2 = " +number2)

// Question 5
let year;
let yearNot2015Or2016;

year = 2000;

yearNot2015Or2016 = (year !== 2015) && (year !== 2016);

console.log(yearNot2015Or2016);