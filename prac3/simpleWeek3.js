//week 3 practical

question1();
question2();
question4();

//question 1 answer
function question1() {
    let output = "";

    var testObj = {
        number: 1,
        string: "abc",
        array: [5, 4, 3, 2, 1],
        boolean: true
    };

    function objectToHTML(obj) {
        var result = "";
        for (var property in obj) {
            result += property + ": " + obj[property] + "<br/>";
        }
        return result;
    }

    output += objectToHTML(testObj);

    let outPutArea = document.getElementById("outputArea1");
    outPutArea.innerHTML = output;
};

//question 2 answer
function question2() {

    var outputAreaRef = document.getElementById("outputArea2");

    var output = "";

    function flexible(fOperation, operand1, operand2) {
        var result = fOperation(operand1, operand2);
        return result;
    }

    function plus(num1, num2) {
        let total = num1 + num2
        return total
    }

    function multiply(num1, num2) {
        let total = num1 * num2
        return total
    }

    output += flexible(plus, 3, 5) + "<br/>";
    output += flexible(multiply, 3, 5) + "<br/>";

    outputAreaRef.innerHTML = output;

};

//question 3 answer
/**
Pseudocode:

make function called extremeValues with 1 parameter

    make a variable min to put the minimum value
    put the first value of the array (parameter) inside the variable min
    
    make a variable max to put the maximum value
    put the first value of the array (parameter) inside the variable max
    
    make for loop to go through the array (parameter)
        if index i+1 < index i
            replace the value in min variable with index i+1
        else if index i+1 > index i
            replace the value in max variable with index i+1
    
    return max and min in single variable
    
end of the function
**/

//question 4 answer
function question4() {

    let output = "";

    var values = [4, 3, 6, 12, 1, 3, 8];

    function extremeValues(myArray) {

        let min = myArray[0]
        let max = myArray[0]

        for (let i = 0; i < myArray.length; i++) {
            if (myArray[i] < min) {
                min = myArray[i]
            } else if (myArray[i] > max) {
                max = myArray[i]
            }
        }

        return result = [max, min]

    }

    extremeValues(values)

    output += result[0] + "<br\>";
    output += result[1];

    let outPutArea = document.getElementById("outputArea3");
    outPutArea.innerHTML = output;
};